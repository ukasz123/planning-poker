import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_router/angular_router.dart';
import '../routes.dart';
import 'game_form_service.dart';
import '../model/round.dart';
import 'round_component.dart';
import 'round_form_component.dart';

@Component(
  selector: "game-form",
  templateUrl: 'game_form_component.html',
  directives: [
    materialInputDirectives,
    NgFor,
    NgIf,
    RoundFormComponent,
    RoundComponent,
    MaterialListComponent,
    MaterialListItemComponent,
    MaterialButtonComponent,
    MaterialIconComponent,
  ],
  providers: [ClassProvider(GameFormService), materialProviders],
)
class GameFormComponent implements OnInit, OnDestroy {
  final GameFormService _gameFormService;
  final Router _router;

  StreamSubscription<List<Round>> _roundSubscription;

  get service => _gameFormService;

  String name = '';
  bool nameSet = false;

  List<Round> rounds = <Round>[];

  GameFormComponent(this._gameFormService, this._router);

  Future<void> setName() async {
    await _gameFormService.setTitle(name);
    name = await _gameFormService.name;
    nameSet = true;
  }

  Future<void> save() async {
    await _gameFormService.saveData();
    await _router.navigate(RoutePaths.listOfGames.path);
  }

  @override
  Future<void> ngOnInit() async {
    name = await _gameFormService.name;
    _roundSubscription ??=
        _gameFormService.rounds.listen((data) => this.rounds = data);
  }

  @override
  void ngOnDestroy() {
    _roundSubscription?.cancel();
    _roundSubscription = null;
  }
}
