import 'dart:async';

import 'package:angular/angular.dart';
import '../auth_service.dart';
import '../db/data_provider_service.dart';
import '../model/game.dart';

@Injectable()
class GameListService {
  final DataProvider _provider;
  final AuthService _authService;
  GameListService(this._provider, this._authService);
  Stream<List<Game>> get games => _provider.gamesForUser(_authService.uid);
}
