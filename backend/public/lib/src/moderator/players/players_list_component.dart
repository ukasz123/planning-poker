import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:public/src/model/player.dart';

@Component(
    selector: 'mod-players',
    directives: [
      NgFor,
      MaterialChipsComponent,
      MaterialChipComponent,
    ],
    templateUrl: 'players_list_component.html',
    pipes: [
      AsyncPipe,
    ])
class PlayersListComponent {
  @Input('players')
  Stream<List<Player>> players;
}
