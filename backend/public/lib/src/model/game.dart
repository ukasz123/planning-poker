import 'package:public/src/model/player.dart';

import 'round.dart';

class Game {
  final String title;

  List<Round> rounds;
  final String id;

  final String invitationId;

  Game(this.title, {rounds, this.id = null, this.invitationId = null})
      : this.rounds = rounds ?? [];
}
