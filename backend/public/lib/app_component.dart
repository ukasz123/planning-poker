import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:firebase_dart_ui/firebase_dart_ui.dart';

import 'src/auth_service.dart';
import 'src/db/data_provider_service.dart';
import 'src/environment.dart';
import 'src/mode_chooser_component.dart';
import 'src/remote_functions.dart';
import 'src/routes.dart';

// AngularDart info: https://webdev.dartlang.org/angular
// Components info: https://webdev.dartlang.org/components

@Component(
  selector: 'app',
  templateUrl: 'app_component.html',
  directives: [
    routerDirectives,
    ModeChooserComponent,
    NgIf,
    FirebaseAuthUIComponent,
  ],
  exports: [
    RoutePaths,
    Routes,
    AsyncPipe,
  ],
  providers: [
    ClassProvider(AuthService),
    ClassProvider(DataProvider),
    ClassProvider(RemoteFunctions)
  ],
)
class AppComponent implements OnInit, OnDestroy {
  AuthService _authService;
  List<StreamSubscription> _subs = [];

  AppComponent(this._authService) {}

  String displayName = "";
  bool authenticated = false;

  UIConfig get uiConfig => _authService.uiConfig;

  Future<void> logout() async {
    await _authService.logout();
  }

  @override
  void ngOnInit() {
    print('ngOnInit');
    initializeApp();
    if (_subs.isEmpty) {
      _subs.add(_authService.isAuthenticated.listen((authenticated) {
        print('on authenticated: $authenticated');
        this.authenticated = authenticated;
      }));
      _subs.add(_authService.displayName.listen((name) {
        this.displayName = name;
      }));
    }
  }

  @override
  void ngOnDestroy() {
    print('ngOnDestroy');
    _subs.forEach((s) => s.cancel());
    _subs.clear();
  }
}
