class Round {
  final String title;
  final String description;

  Round(this.title, {this.description = null});
}

class RoundWithState extends Round {
  RoundWithState(String title, {String description})
      : super(title, description: description);
  RoundState _state = RoundState.firstBid;

  RoundState get state => _state;

  set state(RoundState newState) {
    _state = newState;
  }

  bool active = false;
  int value;
  int votesCount = 0;
  int median;
  double average;

  String id;

  bool get done => state == RoundState.done;

  @override
  String toString() {
    return 'RoundWithState{state: $_state, active: $active, value: $value, votesCount: $votesCount, median: $median, id: $id}';
  }
}

enum RoundState {
  firstBid,
  discussion,
  secondBid,
  finalResults,
  done,
}
