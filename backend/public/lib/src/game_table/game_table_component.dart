import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/model/action/async_action.dart';
import 'package:angular_components/utils/angular/scroll_host/angular_2.dart';
import 'package:angular_router/angular_router.dart';
import '../auth_service.dart';
import 'package:rxdart/rxdart.dart';
import '../model/player.dart';
import '../model/vote.dart';
import '../db/data_provider_service.dart';

import '../model/round.dart' show RoundState, RoundWithState;
import 'bid_component.dart';
import 'game_table_service.dart';

@Component(
  selector: 'game-table',
  templateUrl: 'game_table_component.html',
  directives: [
    NgFor,
    NgIf,
    BidComponent,
    MaterialToggleComponent,
    MaterialStepperComponent,
    MaterialListComponent,
    MaterialListItemComponent,
    MaterialExpansionPanelSet,
    MaterialExpansionPanel,
    StepDirective,
    SummaryDirective,
  ],
  providers: [
    scrollHostProviders,
    ClassProvider(GameTableService),
    materialProviders,
  ],
  pipes: [
    AsyncPipe,
    DecimalPipe,
  ],
)
class GameTableComponent implements OnInit, OnDestroy {
  final GameTableService _service;
  final DataProvider _provider;
  final AuthService _authService;

  Stream<List<RoundWithState>> _rounds;

  Map<String, Stream<List<PlayersVote>>> _currentVotes = {};

  StreamSubscription<List<RoundWithState>> _stepJumpingSub;

  GameTableComponent(this._service, this._provider, this._authService);

  @Input('rounds')
  void set rounds(Stream<List<RoundWithState>> stream) {
    this._rounds = stream.asBroadcastStream();
//    this._stepJumpingSub = _rounds.listen((list) {
//      int firstActive = list.indexWhere((round) => round.active);
//      if (firstActive != 0) {
//        stepper.jumpStep(firstActive);
//      }
//    });
    print('Setting rounds stream: $_rounds');
    this._currentVotes = {};
  }

  Stream<List<RoundWithState>> get rounds => this._rounds;

  @Input('gameId')
  String gameId;

  @Input('modPlaying')
  bool modIsPlaying = false;

//  @ViewChild('gamestepper')
//  MaterialExpansionPanelSet stepper;

  Stream<List<PlayersVote>> currentVotes(String roundId) {
    return _currentVotes.putIfAbsent(
        roundId, () => _createCurrentVotes(roundId));
  }

  @override
  void ngOnInit() {
//    stepper.
  }

  @override
  void ngOnDestroy() {
    _stepJumpingSub?.cancel();
    _stepJumpingSub == null;
  }

  Future<void> onBid(RoundWithState round, int value) async {
    print('onBid $round => $value');
    await _provider.saveVote(
        gameId: gameId,
        roundId: round.id,
        voterId: _authService.uid,
        value: value);
  }

  void continueToNextRound(AsyncAction<bool> action, RoundWithState round) {
    print('Continue? ${round.done}');
    if (!canContinue(round)) {
      action.cancel();
    } else {
      print('Setting value to ${round.median}');
      _provider.setDone(gameId, round.id, round.median);
//      round.active = false;
//      round.state == RoundState.done;
//      int nextIndex = rounds.indexOf(round) + 1;
//      if (nextIndex < rounds.length) {
//        print('moving to $nextIndex round');
//        rounds[nextIndex].active = true;
//      }
    }
  }

  void discussionFinished(RoundWithState round) {
    print('Discussion finished. Moving on');
    _provider.setDiscussionFinished(gameId, round.id);
  }

  bool canContinue(RoundWithState round) => round.state != RoundState.firstBid;

  bool isDiscussion(RoundWithState round) =>
      round.state == RoundState.discussion;

  Stream<List<PlayersVote>> _createCurrentVotes(String roundId) {
    final currentVotesList =
        _provider.votesForRound(gameId, roundId).map((votes) {
      print('Current votes: ${votes.join(',')}');
      return votes;
    }).asBroadcastStream();
    final currentPlayers = this
        ._provider
        .playersForGame(gameId)
        .asBroadcastStream()
        .map((players) {
      print('Current players: ${players.join(',')}');
      return players;
    });
    return Observable.combineLatest2(currentVotesList, currentPlayers,
        (List<Vote> votes, List<Player> players) {
      return votes
          .map((vote) {
            final player = players.firstWhere((p) => p.id == vote.voterId);
            if (player != null) {
              return PlayersVote(player, vote);
            } else {
              return null;
            }
          })
          .where((d) => d != null)
          .toList();
    }).map((votes) {
      print('Combined votes: ${votes.join(' | ')}');
      return votes;
    }).asBroadcastStream(onListen: (sub) {
      print('Votes list stream subscribed: $sub:${sub.hashCode}');
    }).shareReplay(maxSize: 1);
  }
}

class PlayersVote {
  final Player player;
  final Vote vote;

  PlayersVote(this.player, this.vote);
}
