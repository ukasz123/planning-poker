import 'package:angular/angular.dart';
import 'package:firebase/firebase.dart' as fb;
import 'auth_service.dart';
import 'package:http/browser_client.dart';

const String _functionsBaseUrl =
    'https://us-central1-planning-poker-f529f.cloudfunctions.net';
const int _HTTP_OK_CODE = 200;

@Injectable()
class RemoteFunctions {
  final AuthService _authService;
  final _client = BrowserClient()..withCredentials = true;

  RemoteFunctions(this._authService);

  Future<bool> startTheGame(String gameId) async {
    final headers = await _authHeaders;
    final startGameUrl = '$_functionsBaseUrl/startGame';
    print('Calling $startGameUrl with id = $gameId');

    return await _client
        .post(startGameUrl,
            body: Map<String, String>.of({'id': gameId}), headers: headers)
        .then((response) {
      print('Got response ${response.statusCode} -> ${response.reasonPhrase}');
      return response.statusCode == _HTTP_OK_CODE;
    }, onError: (e) {
      print('Something failed: $e');
      return false;
    });
  }

  Future<String> acceptInvitation(String invitationId) async {
    final headers = await _authHeaders;
    final acceptInvitationUrl = '$_functionsBaseUrl/accept/$invitationId';
    final response = await _client.get(acceptInvitationUrl, headers: headers);
    print('Got response ${response.statusCode} -> ${response.reasonPhrase}');
    return response.body.trim();
  }

  Future<bool> doneWaiting(String gameId) async {
    final headers = await _authHeaders;
    final doneWaitingUrl = '$_functionsBaseUrl/stopWaiting';
    return await _client
        .post(doneWaitingUrl,
            body: Map<String, String>.of({'id': gameId}), headers: headers)
        .then((response) {
      print('Got response ${response.statusCode} -> ${response.reasonPhrase}');
      return response.statusCode == _HTTP_OK_CODE;
    }, onError: (e) {
      print('Something failed: $e');
      return false;
    });
  }

  Future<Map<String, String>> get _authHeaders =>
      _authService.idToken.then((token) => {'Authorization': 'Bearer $token'});
}
