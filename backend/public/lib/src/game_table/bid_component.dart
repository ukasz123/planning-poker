import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/utils/color/color.dart';

@Component(
  selector: 'bid',
  templateUrl: 'bid_component.html',
  directives: [
    NgFor,
    ScorecardComponent,
    ScoreboardComponent,
  ],
)
class BidComponent {
  final ScoreboardType radio = ScoreboardType.radio;
  final ScoreboardType standard = ScoreboardType.standard;
  final Color selectedCardColor = Color.fromHex("#409040");
  final _bidController = StreamController<int>();

  static const _kBiddingValuesLength = 6;
  @Output('bidAction')
  Stream<int> get bids => _bidController.stream;
  @Input('value')
  int value;

  @Input('enabled')
  bool enabled = true;

  List availableBids = List.generate(_kBiddingValuesLength + 2, _fib)
      .skip(2)
      .toList()
        ..insert(0, 0);

  void selectBid(int bid) {
    value = bid;
    if (enabled) {
      _bidController.add(value);
    }
  }

  static int _fib(int index) {
    if (index == 0) {
      return 0;
    }
    if (index == 1) {
      return 1;
    }
    return _fib(index - 2) + _fib(index - 1);
  }
}
