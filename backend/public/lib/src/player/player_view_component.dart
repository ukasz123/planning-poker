import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/material_chips/material_chip.dart';
import 'package:angular_components/material_chips/material_chips.dart';
import 'package:angular_router/angular_router.dart';
import 'package:public/src/model/game.dart';
import 'package:public/src/model/round.dart';
import 'package:public/src/player/player_active_round_component.dart';
import '../db/data_provider_service.dart';
import '../auth_service.dart';
import '../route_paths.dart';

@Component(
  selector: 'player-view',
  templateUrl: 'player_view_component.html',
  directives: [
    NgFor,
    NgIf,
    ActiveRoundComponent,
    MaterialChipsComponent,
    MaterialChipComponent,
    MaterialIconComponent,
    displayNameRendererDirective,
  ],
  styleUrls: ['player_view_component.scss.css'],
  pipes: [AsyncPipe],
)
class PlayerViewComponent implements OnActivate {
  final DataProvider _dataProvider;
  final AuthService _authService;

  PlayerViewComponent(this._dataProvider, this._authService);

  String gameId;

  Game _game;

  Stream<List<RoundWithState>> _rounds =
      Stream<List<RoundWithState>>.empty().asBroadcastStream();

  String get gameTitle => _game?.title ?? '';

  Stream<RoundWithState> _activeRound;

  Stream<RoundWithState> get activeRound {
    if (_activeRound == null) {
      _activeRound = _rounds.map((roundsList) =>
          roundsList.firstWhere((r) => r.active, orElse: () => null));
    }
    return _activeRound;
  }

  Stream<List<RoundWithState>> get rounds => _rounds;

  String get playerId => _authService.uid;

  @override
  void onActivate(RouterState previous, RouterState current) {
    this.gameId = RoutePaths.getGameId(current.parameters);
    _dataProvider.gameForId(gameId).then((game) {
      this._game = game;
    });

    this._rounds = _dataProvider.gameRounds(gameId).asBroadcastStream();
    this._activeRound = null;
  }
}
