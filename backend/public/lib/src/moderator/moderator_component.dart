import 'dart:async';
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_router/angular_router.dart';
import 'package:rxdart/rxdart.dart';
import '../auth_service.dart';
import '../model/round.dart';
import '../game_table/game_table_component.dart';
import '../model/game.dart';
import '../model/player.dart';
import '../moderator/players/players_list_component.dart';
import '../routes.dart';
import '../db/data_provider_service.dart';
import '../remote_functions.dart';

@Component(
  selector: 'moderator',
  templateUrl: 'moderator_component.html',
  directives: [
    routerDirectives,
    PlayersListComponent,
    NgSwitch,
    NgSwitchWhen,
    NgSwitchDefault,
    MaterialButtonComponent,
    MaterialIconComponent,
    MaterialInputComponent,
    GameTableComponent,
  ],
  exports: [RoutePaths, Routes],
  pipes: [AsyncPipe, StateDisplayPipe],
)
class ModeratorComponent implements OnActivate {
  final Router _router;
  final DataProvider _provider;
  final RemoteFunctions _remoteFunctions;
  final AuthService _authService;

  String _gameId = null;

  Game game;

  Stream<List<Player>> players = Stream.fromIterable([[]]);

  Stream<String> state =
      Stream<String>.fromIterable(['init']).asBroadcastStream();

  bool playingToo = false;

  String get invitationUrl =>
      "${window.location.protocol}//${window.location.host}${RoutePaths.acceptInvitation.toUrl(parameters: {
        gameIdParam: game?.invitationId
      })}";

  Stream<List<RoundWithState>> rounds;

  ModeratorComponent(
      this._router, this._provider, this._remoteFunctions, this._authService);

  @override
  void onActivate(RouterState previous, RouterState current) {
    this._gameId = RoutePaths.getGameId(current.parameters);
    print('OnActivate gameId = $_gameId');
    _provider.gameForId(_gameId).then((game) {
      this.game = game;
      print('OnActivate game loaded = ${game.title}');
      _loadState();
      _loadPlayers();
      _loadRounds();
    });
  }

  Future<void> makeMePlay() async {
    await _remoteFunctions.acceptInvitation(game.invitationId);
  }

  Future<void> doneWaiting() async {
    await _remoteFunctions.doneWaiting(_gameId);
  }

  void _loadPlayers() {
    this.players = _provider.playersForGame(_gameId).map((list) {
//      print('Players: ${list.join()}');
      //WARNINIG side effect!!!
      this.playingToo = list == null ||
          list.firstWhere((p) => p.id == _authService.uid,
                  orElse: () => null) !=
              null;
      return list;
    });
  }

  void _loadState() {
    this.state = Observable(_provider.gameState(_gameId).asBroadcastStream())
        .shareReplay(maxSize: 1);
  }

  void _loadRounds() {
    this.rounds = Observable(_provider.gameRounds(_gameId).asBroadcastStream())
        .shareReplay(maxSize: 1);
  }
}

@Pipe('stateDisplay')
class StateDisplayPipe implements PipeTransform {
  transform(dynamic value) => _displayForState('$value');

  String _displayForState(String state) {
    switch (state) {
      case 'awaiting_players':
        return 'Waiting for players';
      case 'playing':
        return 'Playing';
      case 'done':
        return 'Done';
      default:
        return 'Unknown state: $state';
    }
  }
}
