import 'package:firebase/firebase.dart' as fb;

const environment = {
  'firebase': {
    "apiKey": "AIzaSyDNI9mhV0FgYI_Hgdwa3ayaBYRCuIsfnfA",
    "authDomain": "planning-poker-f529f.firebaseapp.com",
    "databaseURL": "https://planning-poker-f529f.firebaseio.com",
    "projectId": "planning-poker-f529f",
    "storageBucket": "planning-poker-f529f.appspot.com",
    "messagingSenderId": "427740221803",
  }
};

fb.App _theApp;

fb.App initializeApp() {
  return app;
}

fb.App get app {
  if (_theApp == null) {
    final Map<String, String> firebaseConfig = environment['firebase'];

    _theApp = fb.initializeApp(
      apiKey: firebaseConfig['apiKey'],
      authDomain: firebaseConfig['authDomain'],
      databaseURL: firebaseConfig['databaseURL'],
      messagingSenderId: firebaseConfig['messagingSenderId'],
      projectId: firebaseConfig['projectId'],
      storageBucket: firebaseConfig['storageBucket'],
    );
    print('Firebase App created: $_theApp');
  }
  return _theApp;
}
