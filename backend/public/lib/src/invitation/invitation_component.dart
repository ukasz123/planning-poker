import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:public/src/remote_functions.dart';
import 'package:public/src/route_paths.dart';
import 'package:public/src/routes.dart';

@Component(
  selector: 'invitation',
  template: '''
  <p *ngIf="!failed">Accepting invitation</p>
  <strong *ngIf="failed">Accepting invitation failed</strong>
  ''',
  directives: [NgIf],
)
class InvitationComponent implements OnActivate {
  final RemoteFunctions _remoteFunctions;
  final Router _router;

  bool failed = false;

  InvitationComponent(this._remoteFunctions, this._router);

  @override
  void onActivate(RouterState previous, RouterState current) {
    final invitation = RoutePaths.getGameId(current.parameters);
    var countdown = 3;
    final invitationAttemptBuilder = () =>
        _remoteFunctions.acceptInvitation(invitation).then((gameId) {
          _router.navigate(
              RoutePaths.playerView.toUrl(parameters: {gameIdParam: gameId}));
          return false;
        }, onError: (_) => true);

    Future.doWhile(() {
      if (countdown == 0) {
        return Future.error('Unable to accept invitation');
      } else {
        countdown--;
        return invitationAttemptBuilder();
      }
    }).catchError((_) {
      failed = true;
      return;
    });
  }
}
