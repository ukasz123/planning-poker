import 'dart:async';

import 'package:angular/core.dart';

/// Mock service emulating access to a to-do list stored on a server.
@Injectable()
class TodoListService {
  List<String> mockTodoList = <String>[];

  Future<List<String>> getTodoList() async => mockTodoList;

  Future<Null> add(String item) async {
    mockTodoList.add(capitalize(item));
  }
}

String capitalize(String item) {
  return item
      .split(' ')
      .map((word) => word.length > 0
          ? word.substring(0, 1).toUpperCase() +
              ((word.length > 1) ? word.substring(1) : '')
          : '')
      .join(' ');
}
