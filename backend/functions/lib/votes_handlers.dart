import 'package:firebase_functions_interop/firebase_functions_interop.dart';
import 'package:planning_poker_functions/common.dart';

ChangeEventHandler<DocumentSnapshot> medianCalculate = (data, context) async {
  print('medianCalculate: ${context.authType} => ${context.eventType}');
  final newData = data.after;
  final newValue = newData.data.getInt('value');
  final votesCollection = newData.reference.parent;
  final roundDocument = votesCollection.parent;
  final currentVotesSnapshot = await votesCollection.get();
  final currentVotes = currentVotesSnapshot.documents
      .map((d) => Pair<String, int>(d.documentID, d.data.getInt('value')))
      .where((vote) => vote.first != newData.documentID)
      .followedBy([Pair<String, int>(newData.documentID, newValue)])
      .map((vote) => vote.second)
      .where((vote) => vote != null)
      .toList();
  if (currentVotes.isNotEmpty) {
    print('Current votes: ${currentVotes.join(',')}');
    currentVotes.sort();
    final int centerIndex = (currentVotes.length / 2).floor();
    final median = currentVotes[centerIndex];
    final average =
        currentVotes.fold(0, (sum, value) => sum + value) / currentVotes.length;
    await roundDocument.updateData(UpdateData.fromMap({
      'votesCount': currentVotes.length,
      'median': median,
      'average': average,
    }));
  }
};

ChangeEventHandler<DocumentSnapshot> biddingFinishedCheck = (data, context) async {
print('biddingFinishedCheck: ${context.authType} => ${context.eventType}');
  final newData = data.after;
  final gameId = context.params['gameId'];
  final currentState = newData.data.getString('state');
  if (currentState == 'firstBid'|| currentState == 'secondBid'){
    final votesCount = newData.data.getInt('votesCount') ?? 0;
    final gameData = await newData.firestore.document('/games/$gameId').get();
    final players = gameData.data.getList('players');
    if (votesCount >= players.length){
      print('Bidding finished!');
      final newState = UpdateData.fromMap({'state': (currentState == 'firstBid') ? 'discussion' : 'finalResults'});
      await data.after.reference.updateData(newState);
    }
  }
};