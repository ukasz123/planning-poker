import 'package:firebase_admin_interop/firebase_admin_interop.dart';
import 'package:planning_poker_functions/common.dart';
import 'package:uuid/uuid.dart';

final AuthorizedRequest startHandler = (request, idToken, app) async {
  String gameId = (request.body as Map<String, dynamic>)['id'];
  final gameDocument = app.firestore().document('/games/$gameId');
  final currentDataSnapshot = await gameDocument.get();
  final currentData = currentDataSnapshot.data;
  if (currentData.getString('ownerId') != idToken.uid) {
    request.response.statusCode = 403;
    request.response.reasonPhrase = 'Operation forbidden';
    return;
  }
  String invitationId = currentData.getString('invitationId') ?? Uuid().v4().toString();
  String invitationUrl = request.uri.replace(path: 'accept/$invitationId').toString();
  UpdateData update = UpdateData.fromMap({
    'started': true,
    'state': currentData.getString('state') ?? 'awaiting_players',
    'invitationId': invitationId,
    'invitationUrl': invitationUrl,
    'players': [],
  });
  await gameDocument.updateData(update);
  print('Game started -> invitation: $invitationUrl');
  request.response.statusCode = 200;
};

final AuthorizedRequest doneWaitingHandler = (request, idToken, app) async {
  String gameId = (request.body as Map<String, dynamic>)['id'];
  final gameDocument = app.firestore().document('/games/$gameId');
  final currentDataSnapshot = await gameDocument.get();
  final currentData = currentDataSnapshot.data;
  if (currentData.getString('ownerId') != idToken.uid) {
    request.response.statusCode = 403;
    request.response.reasonPhrase = 'Operation forbidden';
    return;
  }
  await gameDocument.firestore.runTransaction((transaction) async {
    final roundsQuery = await transaction.getQuery(gameDocument.collection('rounds').orderBy('index'));
    transaction.update(gameDocument, UpdateData.fromMap({'state':'playing'}));
    transaction.update(roundsQuery.documents.first.reference,UpdateData.fromMap({'state': 'firstBid', 'active': true}));
  });
  request.response.statusCode == 200;
};