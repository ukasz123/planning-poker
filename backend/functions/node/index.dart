import 'package:firebase_functions_interop/firebase_functions_interop.dart';
import 'package:node_io/node_io.dart';
import 'package:planning_poker_functions/common.dart';
import 'package:planning_poker_functions/game_state_handlers.dart';
import 'package:planning_poker_functions/invitation_handlers.dart';
import 'package:planning_poker_functions/round_state_handlers.dart';
import 'package:planning_poker_functions/votes_handlers.dart';
import 'package:uuid/uuid.dart';

App _app;

void main() {
  _app = FirebaseAdmin.instance.initializeApp();
  _app.firestore().settings(FirestoreSettings(timestampsInSnapshots: true));

  // HTTPS
  functions['helloWorld'] = functions.https.onRequest(helloWorld);
  functions['startGame'] =
      functions.https.onRequest(_close(_withCORS(_authorized(startHandler))));
  functions['accept'] = functions.https
      .onRequest(_close(_withCORS(_authorized(acceptInvitation))));
  functions['stopWaiting'] = functions.https
      .onRequest(_close(_withCORS(_authorized(doneWaitingHandler))));

  // FIRESTORE
  functions['calculateMedian'] = functions.firestore
      .document('/games/{gameId}/rounds/{roundId}/votes/{voterId}')
      .onWrite(medianCalculate);
  functions['checkVotingState'] = functions.firestore
      .document('/games/{gameId}/rounds/{roundId}')
      .onUpdate(biddingFinishedCheck);
  functions['prepareRoundState'] = functions.firestore
      .document('/games/{gameId}/rounds/{roundId}')
      .onCreate(prepareStateHandler);
  functions['checkRoundDoneState'] = functions.firestore
      .document('/games/{gameId}/rounds/{roundId}')
      .onUpdate(doneStateHandler);
}

void helloWorld(ExpressHttpRequest request) {
  request.response.writeln('Hello world');
  request.response.close();
}

typedef Future<void> _HttpRequestHandler(ExpressHttpRequest request);

/// Wraps the handler method with the authorization parsing logic
_HttpRequestHandler _authorized(AuthorizedRequest requestHandler) =>
    (ExpressHttpRequest request) async {
      Stopwatch authWatch = Stopwatch()..start();
      String headers = '';
      request.headers.forEach((name, values) {
        headers += '$name: ${values.join()}\n';
      });
      print('Headers received: \n$headers');
      final authorizationHeader =
          request.headers.value(HttpHeaders.authorizationHeader);
      final response = request.response;
      if (authorizationHeader == null ||
          !authorizationHeader.startsWith('Bearer ')) {
        print('No Authorization header found $authorizationHeader');
        await response
          ..statusCode = 401
          ..reasonPhrase = 'No authorization data found'
          ..close();
        return;
      }
      final idToken = authorizationHeader.split('Bearer ')[1];
      if (idToken == null || idToken.isEmpty) {
        print('Invalid token - empty');
        await response
          ..statusCode = 401
          ..reasonPhrase = 'Invalid authorization token'
          ..close();
        return;
      }
      await _app.auth().verifyIdToken(idToken).then((token) async {
        try {
          authWatch.stop();
          await requestHandler(request, token, _app);
        } on Exception catch (e) {
          response.statusCode = 501;
          response.reasonPhrase = e.toString();
        }
        authWatch.start();
        await response.close();
      }, onError: (error) async {
        print('Invalid token - wrong token $idToken -> $error');
        await request.response
          ..statusCode = 401
          ..reasonPhrase = 'Invalid authorization token'
          ..close();
        return;
      });
      authWatch.stop();
      print('Time spend on authorization logic: ${authWatch.elapsedMilliseconds} ms');
    };

const _allowedOrigins = [
  'http://localhost:8080',
  'https://planning-poker-f529f.firebaseapp.com'
];

_HttpRequestHandler _withCORS(_HttpRequestHandler handler) =>
    (ExpressHttpRequest request) async {
      final origin = request.headers.value('Origin');
      if (_allowedOrigins.contains(origin)) {
        final responseHeaders = request.response.headers;
        responseHeaders.set('Access-Control-Allow-Origin', origin);
        responseHeaders.set('Access-Control-Allow-Credentials', 'true');
        responseHeaders.set(
            'Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
        responseHeaders.set("Access-Control-Allow-Headers",
            "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Execution-Id");
        responseHeaders.set('Access-Control-Max-Age', '3600');
        if (request.method.toUpperCase() == 'OPTIONS') {
          print('CORS check request\'s response => 200');
          request.response.statusCode = 200;
        } else {
          await handler(request);
        }
      } else {
        print('Origin $origin denied!');
        request.response.statusCode = 400;
      }
    };

_HttpRequestHandler _close(_HttpRequestHandler handler) =>
    (ExpressHttpRequest request) async {
      final requestId = Uuid().v4().toString();
      final watch = Stopwatch()..start();
            print('Start handling ${request.requestedUri} [$requestId]');
      await handler(request);
      await request.response.close();
      watch.stop();
      print('Done handling ${request.requestedUri} [$requestId] in ${watch.elapsedMilliseconds} ms');
    };
