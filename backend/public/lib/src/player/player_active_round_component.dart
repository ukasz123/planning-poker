import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:public/src/db/data_provider_service.dart';
import 'package:public/src/game_table/bid_component.dart';
import 'package:public/src/model/round.dart';

@Component(
  selector: 'active-round',
  templateUrl: 'player_active_round_component.html',
  directives: [
    BidComponent,
    NgIf,
  ],
  providers: [
    materialProviders,
  ],
)
class ActiveRoundComponent {
  final DataProvider _provider;

  @Input('round')
  RoundWithState round;
  @Input('gameId')
  String gameId;
  @Input('playerId')
  String playerId;

  bool get bidEnabled =>
      round.state == RoundState.secondBid || round.state == RoundState.firstBid;
  ActiveRoundComponent(this._provider);

  Future<void> sendBid(int bidValue) async {
    await _provider.saveVote(
        gameId: gameId, roundId: round.id, voterId: playerId, value: bidValue);
  }
}
