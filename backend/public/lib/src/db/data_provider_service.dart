import 'package:angular/angular.dart';
import 'package:firebase/firebase.dart' as fb;
import 'package:firebase/firestore.dart' as fs;
import '../model/vote.dart';
import '../model/player.dart';
import '../model/game.dart';
import '../model/round.dart';

// database fields
const _kOwnerId = 'ownerId';
const _kTitle = 'title';
const _kInvitationId = 'invitationId';

@Injectable()
class DataProvider {
  fs.Firestore get _store => fb.firestore();

  Future<Game> gameForId(String id) async {
    final document = await _store.doc("/games/$id");
    return await _gameFromDocument(document);
  }

  Future<Game> save(Game game, String ownerId) async {
    var batch = _store.batch();
    fs.DocumentReference gameDocument;
    if (game is _DocumentGame) {
      gameDocument = game._document;
    } else {
      gameDocument =
          await _store.collection('/games').add({_kOwnerId: ownerId});
    }
    batch.set(gameDocument, {_kTitle: game.title}, fs.SetOptions(merge: true));
    for (var index = 0; index < game.rounds.length; index++) {
      final round = game.rounds[index];
      if (round is _DocumentRound) {
        batch.update(round._document,
            data: _DocumentRound._toMap(round)
              ..addAll({'index': index.toString()}));
      } else {
        gameDocument.collection('/rounds').add(
            _DocumentRound._toMap(round)..addAll({'index': index.toString()}));
      }
    }
    ;
    return batch.commit().then((_) => _gameFromDocument(gameDocument));
  }

  Future<Game> _gameFromDocument(fs.DocumentReference document) async {
    final data = await document.get();
    final roundsSnapshot = await document.collection('/rounds').get();
    final game = _DocumentGame(
        document, data, roundsSnapshot.docs.map((s) => _DocumentRound(s)));
    return game;
  }

  Stream<List<Game>> gamesForUser(String uid) {
    return _store
        .collection('/games')
        .where(_kOwnerId, '==', uid)
        .onSnapshot
        .map((snapshot) => snapshot.docs
            .map((document) => _GameWithDocument(document))
            .toList());
  }

  Stream<List<Player>> playersForGame(String gameId) => _store
      .collection('/games/$gameId/players')
      .onSnapshot
      .map((snapshot) => snapshot.docs.map(_playerFromDocument).toList());

  Player _playerFromDocument(fs.DocumentSnapshot doc) {
    return _Player(
      id: doc.id,
      email: doc.get('email'),
      name: doc.get('name'),
      photoUrl: doc.get('photoUrl'),
    );
  }

  Stream<String> gameState(String gameId) => _store
      .doc('/games/$gameId')
      .onSnapshot
      .map((snapshot) => snapshot.get('state') as String);

  Stream<List<RoundWithState>> gameRounds(String gameId) {
    print('get game rounds for $gameId');
    return _store
        .collection('/games/$gameId/rounds')
        .orderBy('index')
        .onSnapshot
        .map((snapshot) {
      return snapshot;
    }).map((snapshot) {
      final list = snapshot.docs
          .map((s) => _roundWithStateFromData(s.id, s.data()))
          .toList();
      return list;
    });
  }

  RoundWithState _roundWithStateFromData(
          String id, Map<String, dynamic> data) =>
      RoundWithState(data['title'], description: data['description'])
        ..state = _stateFromString(data['state'])
        ..active = data['active'] ?? false
        ..value = data['value']
        ..votesCount = data['votesCount'] ?? 0
        ..median = data['median']
        ..average = data['average']
        ..id = id;

  Stream<List<Vote>> votesForRound(String gameId, String roundId) {
    print('votesForRound: $gameId, $roundId');
    return _store
        .collection('/games/$gameId/rounds/$roundId/votes')
        .onSnapshot
        .map((snapshot) => snapshot.docs)
        .map((docs) => docs.map(_voteFromDoc).toList())
        .map((votes) {
      return votes;
    });
  }

  Vote _voteFromDoc(fs.DocumentSnapshot doc) =>
      Vote(value: doc.get('value'), voterId: doc.id);

  Future<void> saveVote(
      {String gameId, String roundId, String voterId, int value}) async {
    await _store
        .doc("/games/$gameId/rounds/$roundId/votes/$voterId")
        .set({'value': value});
  }

  Future<void> setDone(String gameId, String roundId, int value) async {
    await _store.doc('/games/$gameId/rounds/$roundId').update(data: {
      'state': 'done',
      'value': value,
    });
  }

  Future<void> setDiscussionFinished(String gameId, String roundId) =>
      _store.doc('/games/$gameId/rounds/$roundId').update(data: {
        'state': 'secondBid',
      });
}

RoundState _stateFromString(String state) {
  if (state == null) {
    return RoundState.firstBid;
  }
  switch (state) {
    case 'firstBid':
      return RoundState.firstBid;
    case 'discussion':
      return RoundState.discussion;
    case 'secondBid':
      return RoundState.secondBid;
    case 'finalResults':
      return RoundState.finalResults;
    case 'done':
      return RoundState.done;
  }
  return null;
}

class _DocumentGame extends Game {
  final fs.DocumentReference _document;

  _DocumentGame(
      this._document, fs.DocumentSnapshot data, Iterable<_DocumentRound> rounds)
      : super(data.get(_kTitle),
            rounds: rounds.toList(),
            id: _document.id,
            invitationId: data.get(_kInvitationId));
}

class _DocumentRound extends Round {
  fs.DocumentReference _document;

  _DocumentRound(fs.DocumentSnapshot snapshot)
      : super(snapshot.data()[_kTitle],
            description: snapshot.data()['description']) {
    this._document = snapshot.ref;
  }

  static Map<String, String> _toMap(Round r) => {
        _kTitle: r.title,
        'description': r.description,
      };
}

class _GameWithDocument extends Game {
  fs.DocumentSnapshot _snapshot;

  _GameWithDocument(this._snapshot)
      : super(_snapshot.data()[_kTitle], id: _snapshot.id);
}

class _Player extends Player {
  final String id;
  final String email;
  final String name;
  final String photoUrl;

  _Player({this.id, this.email, this.name, this.photoUrl});
}
