import 'package:angular/angular.dart';
import '../model/round.dart';

@Component(
  selector: 'round',
  templateUrl: 'round_component.html',
)
class RoundComponent {
  @Input('data')
  Round round;
}
