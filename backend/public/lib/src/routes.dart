import 'package:angular_router/angular_router.dart';

import 'game_form/game_form_component.template.dart' as game_form_template;
import 'game_list/game_list_component.template.dart' as game_list_template;
import 'mode_chooser_component.template.dart' as mode_chooser_template;
import 'player/player_view_component.template.dart' as player_view_template;
import 'invitation/invitation_component.template.dart' as invitation;
import 'moderator/moderator_component.template.dart' as moderator;
import 'route_paths.dart';

export 'route_paths.dart';

class Routes {
  static final gameForm = RouteDefinition(
    routePath: RoutePaths.gameForm,
    component: game_form_template.GameFormComponentNgFactory,
  );

  static final pickMode = RouteDefinition(
    routePath: RoutePaths.pickMode,
    component: mode_chooser_template.ModeChooserComponentNgFactory,
    useAsDefault: true,
  );

  static final listOfGames = RouteDefinition(
    routePath: RoutePaths.listOfGames,
    component: game_list_template.GameListComponentNgFactory,
  );
  static final moderatorView = RouteDefinition(
    routePath: RoutePaths.moderatorView,
    component: moderator.ModeratorComponentNgFactory,
  );

  static final acceptInvitation = RouteDefinition(
    routePath: RoutePaths.acceptInvitation,
    component: invitation.InvitationComponentNgFactory,
  );
  static final playerView = RouteDefinition(
    routePath: RoutePaths.playerView,
    component: player_view_template.PlayerViewComponentNgFactory,
  );

  static final all = [
    gameForm,
    pickMode,
    listOfGames,
    playerView,
    acceptInvitation,
    moderatorView,
  ];

  static final moderatorRoutes = [];
}
