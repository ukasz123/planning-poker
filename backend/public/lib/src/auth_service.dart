import 'dart:async';

import 'package:js/js.dart';
import 'package:firebase_dart_ui/firebase_dart_ui.dart';
import 'package:firebase/src/interop/firebase_interop.dart';

import 'package:firebase/firebase.dart' as fb;

class AuthService {
  AuthService() {
    print('AuthService constructor');
    _userStream = _userData.stream.asBroadcastStream();
    _userData.add(null);
  }

  UIConfig get uiConfig {
    if (_uiConfig == null) {
      print('Building UI Config');
      _uiConfig = UIConfig(
        signInOptions: [
          // Google OAuth
          CustomSignInOptions(
            provider: fb.GoogleAuthProvider.PROVIDER_ID,
            scopes: ['profile', 'email'],
          ),
          // GitHub OAuth
          CustomSignInOptions(
            provider: fb.GithubAuthProvider.PROVIDER_ID,
            scopes: ['read:user'],
          ),
          fb.EmailAuthProvider.PROVIDER_ID,
        ],
        credentialHelper: GOOGLE_YOLO,
        callbacks: Callbacks(
          signInSuccessWithAuthResult: allowInterop(_signInSuccessCallback),
          signInFailure: (AuthUIError error) {
            print('Auth failured: $error');
            _userData.add(null);
            return PromiseJsImpl<void>(() => print("Sign In Failuer"));
          },
        ),
        tosUrl: '/tos.html',
      );
      // here comes the side effect! yay!
      _userData.add(fb.auth().currentUser);
      fb.auth().onAuthStateChanged.listen(_userData.add);
      fb.auth().onAuthStateChanged.listen((user) => _uid = user?.uid);
    }
    return _uiConfig;
  }

  Stream<String> get email => _userStream.map((user) => user?.email);

  Stream<bool> get isAuthenticated => _userStream.map((user) => user != null);

  Stream<String> get displayName => _userStream.map(_userName);

  String get uid => _uid;

  Future<String> get idToken => fb.auth().currentUser.getIdToken();

  // private
  UIConfig _uiConfig;

  String _uid;

  String _userName(fb.User user) => user?.displayName ?? user?.email;

  final StreamController<fb.User> _userData = StreamController();
  Stream<fb.User> _userStream;

  bool _signInSuccessCallback(
      fb.UserCredential credential, String redirectUrl) {
    print(
        'On signed in: ${credential.credential.providerId}/${_userName(credential.user)} -> $redirectUrl');
    _userData.add(credential.user);

    return false;
  }

  Future<bool> logout() async {
    await fb.auth().signOut();
    return true;
  }
}
