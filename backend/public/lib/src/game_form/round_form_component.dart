import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'dart:async';

import 'game_form_service.dart';

@Component(
  selector: "round-form",
  templateUrl: 'round_form_component.html',
  directives: [
    MaterialButtonComponent,
    MaterialIconComponent,
    materialInputDirectives,
    MaterialMultilineInputComponent,
  ],
  providers: [materialProviders],
)
class RoundFormComponent {
  @Input('service')
  GameFormService gameService;

  String title;
  String description;

  Future<void> addRound() async {
    await gameService.addRound(title, description: description);
    title = '';
    description = '';
  }
}
