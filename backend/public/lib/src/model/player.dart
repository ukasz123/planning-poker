abstract class Player {
  String get id;
  String get name;
  String get email;

  @override
  String toString() => "Player($name, $email)";
}
