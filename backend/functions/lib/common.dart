import 'package:firebase_functions_interop/firebase_functions_interop.dart';

const String functionsBaseUrl = 'https://us-central1-planning-poker-f529f.cloudfunctions.net';

/// Request handler that requires authorization
typedef Future<void> AuthorizedRequest(
    ExpressHttpRequest request, DecodedIdToken idToken, App app);

/// Simple request handler that uses app to retrieve some data
typedef Future<void> SimpleRequest(ExpressHttpRequest request, App app);

class Pair<F,S> {
  final F first;
  final S second;

  Pair(this.first, this.second);

  String toString() => '($first : $second)';
}