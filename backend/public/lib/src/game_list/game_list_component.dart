import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_router/angular_router.dart';
import '../remote_functions.dart';
import '../model/game.dart';
import 'game_list_service.dart';
import '../routes.dart';
import '../game_table/bid_component.dart';

@Component(
  selector: 'game-list',
  templateUrl: 'game_list_component.html',
  directives: [
    NgFor,
    MaterialListComponent,
    MaterialListItemComponent,
    MaterialButtonComponent,
    MaterialIconComponent,
    routerDirectives,
    BidComponent,
  ],
  providers: [ClassProvider(GameListService), materialProviders],
  exports: [RoutePaths, Routes],
)
class GameListComponent implements OnActivate, OnDeactivate {
  final GameListService _service;
  final Router _router;
  final RemoteFunctions _remoteApi;

  GameListComponent(this._service, this._router, this._remoteApi);

  List<Game> games = [];
  StreamSubscription<List<Game>> _sub;

  @override
  void onActivate(RouterState previous, RouterState current) {
    _sub ??= _service.games.listen((newGames) {
      games = newGames;
    });
  }

  @override
  void onDeactivate(RouterState previous, RouterState current) {
    _sub?.cancel();
    _sub = null;
  }

  Future<NavigationResult> _moderateGame(Game game) async {
    print("Going to the game: ${game.title}");
    final started = await _remoteApi.startTheGame(game.id);
    if (started) {
      return await _router.navigate(_gameUrl(game.id));
    } else {
      return NavigationResult.BLOCKED_BY_GUARD;
    }
  }

  Future<void> onSelect(Game game) async {
    final results = await _moderateGame(game);
    print("Navigation results: ${results.index}");
  }
}

String _gameUrl(String id) => Routes.moderatorView.toUrl({gameIdParam: id});
