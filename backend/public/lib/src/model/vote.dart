class Vote {
  final int value;
  final String voterId;

  Vote({this.value, this.voterId});

  @override
  String toString() {
    return 'Vote{value: $value, voterId: $voterId}';
  }
}
