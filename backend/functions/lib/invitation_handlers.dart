import 'package:firebase_admin_interop/firebase_admin_interop.dart';
import 'package:planning_poker_functions/common.dart';

final AuthorizedRequest acceptInvitation = (request, idToken, app) async {
  final user = await app.auth().getUser(idToken.uid);
  final userEmail = user.email;
  final invitationId = request.uri.pathSegments.last;
  final gamesFound = await app
      .firestore()
      .collection('/games')
      .where('invitationId', isEqualTo: invitationId)
      .get();
  final gameDocumentSnapshot = gamesFound.documents.first;
  final gameDocument = gameDocumentSnapshot.reference;
  final gameData = gameDocumentSnapshot.data;
  final gameState = gameData.getString('state');
  final playersData = gameData.getList('players');
  if (!playersData.contains(userEmail)) {
    if (gameState == 'awaiting_players') {
      final update =
          UpdateData.fromMap({'players': List.of(playersData)..add(userEmail)});
      await gameDocument.updateData(update);
      await gameDocument
          .collection('players')
          .document(idToken.uid)
          .setData(DocumentData.fromMap({
            'email': userEmail,
            'name': user.displayName ?? user.email,
            'photoUrl': user.photoURL,
          }));
    } else {
      request.response.statusCode = 403;
      request.response.reasonPhrase = 'Done waiting';
      return;
    }
  }
  request.response.writeln(gameDocument.documentID);
};
