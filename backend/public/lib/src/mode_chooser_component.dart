import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'routes.dart';

@Component(
  selector: 'mode',
  template: '''
    <h2>Pick mode</h2>
    <div>
    <div class="mode-div">
      <a [routerLink]="RoutePaths.gameForm.toUrl()">Create a Game</a>
    </div>
    <div class="mode-div">
      <a [routerLink]="RoutePaths.listOfGames.toUrl()">Join a Game</a>
    </div>
    </div>
  ''',
  styles: ['.mode-div {width:50%; display:inline-block;}'],
  directives: [routerDirectives],
  exports: [RoutePaths, Routes],
)
class ModeChooserComponent {}
