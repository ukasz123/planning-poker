import 'dart:async';

import 'package:angular/angular.dart';
import '../db/data_provider_service.dart';
import '../auth_service.dart';

import '../model/game.dart';
import '../model/round.dart';

@Injectable()
class GameFormService {
  final DataProvider _dataProvider;
  final AuthService _authService;

  GameFormService(this._dataProvider, this._authService);

  Game _game;

  Future<String> get name async => _game?.title;

  StreamController<bool> _updates = StreamController();
  Stream<List<Round>> get rounds => _updates.stream.map((_) => _game?.rounds);

  Future<void> setTitle(String title) async {
    _game = _game ?? Game(title);
  }

  Future<void> addRound(String title, {String description}) async {
    _game = _game ?? Game('');
    _game.rounds = List.of(_game.rounds)
      ..add(Round(title, description: description));
    _updates.add(true);
  }

  Future<void> saveData() async {
    await _dataProvider.save(_game, _authService.uid);
    print('Game stored');
  }
}
