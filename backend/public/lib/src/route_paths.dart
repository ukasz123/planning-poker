import 'package:angular_router/angular_router.dart';

const gameIdParam = 'gameId';

class RoutePaths {
  static final gameForm = RoutePath(path: 'game_form');

  static final playerView = RoutePath(path: 'play/:$gameIdParam');

  static final pickMode = RoutePath(path: 'mode');

  static final listOfGames = RoutePath(path: 'games');

  static final acceptInvitation = RoutePath(path: 'accept/:$gameIdParam');

  static final moderatorView = RoutePath(path: 'moderate/:$gameIdParam');

  static String getGameId(Map<String, String> parameters) {
    final id = parameters[gameIdParam];
    return id;
  }
}
