import 'package:firebase_functions_interop/firebase_functions_interop.dart';

DataEventHandler<DocumentSnapshot> prepareStateHandler = (data, context) async {
  await data.reference.updateData(UpdateData.fromMap({
    'active': false,
    'done': null,
    'state': 'firstBid',
  }));
};

ChangeEventHandler<DocumentSnapshot> doneStateHandler = (data, context) async {
  final snapshot = data.after;
  if (snapshot.data.getString('state') == 'done'){
    await snapshot.firestore.runTransaction((transaction) async {
      final roundsCollection = snapshot.reference.parent;
      final notDoneRoundsSnapshot = await 
      transaction.getQuery(roundsCollection.where('done',isNull: true ).orderBy('index'));
      print('Found ${notDoneRoundsSnapshot.documents.length} not finished rounds');
      final nextRound = notDoneRoundsSnapshot.documents
        .firstWhere((roundSnapshot) => roundSnapshot.documentID != snapshot.documentID, orElse: ()=>null);
      transaction.update(snapshot.reference, UpdateData.fromMap({
        'active': false,
        'done': true,
      }));
      if (nextRound != null){
      transaction.update(nextRound.reference, UpdateData.fromMap({
        'active': true,
        'state':'firstBid',
      }));
      }
      print('Round done: ${snapshot.documentID}, moving to ${nextRound?.documentID}');
    });
  }
};